package client;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.ArrayList;

import javax.swing.JOptionPane;

public class Connection implements Runnable
{
	BufferedReader in;
	PrintWriter out;

	private static BufferedReader swearReader;

	public static String username = "undefined";
	private static String ip = "stmetadata.populinexu.com";

	private static ArrayList<String> swears;

	private static String regex = "^[A-Za-z0-9]*$";

	Socket s;
	Thread t;

	private static Connection conn;

	public static void init()
	{
		try
		{
			swears = new ArrayList<String>();
			// System.out.println(new
			// File(Connection.class.getResource("Resources/swears.txt").getFile()));
			/*
			 * File folder = new
			 * File(Connection.class.getResource("/Resources").getFile());
			 * File[] listOfFiles = folder.listFiles();
			 * 
			 * for (int i = 0; i < listOfFiles.length; i++) { if
			 * (listOfFiles[i].isFile()) { System.out.println("File " +
			 * listOfFiles[i].getName()); } else if
			 * (listOfFiles[i].isDirectory()) { System.out.println("Directory "
			 * + listOfFiles[i].getName()); } }
			 */
			swearReader = new BufferedReader(
					new InputStreamReader(Connection.class.getResourceAsStream("/Resources/swear.txt")));

			String temp = "";
			while ((temp = swearReader.readLine()) != null)
			{
				swears.add(temp);
			}

			// ip = JOptionPane.showInputDialog("Enter IP Address of Server");
			username = JOptionPane.showInputDialog("Enter a username (May only include letters or numbers)");

			boolean match = valid(username);
			while (!match)
			{
				username = JOptionPane.showInputDialog(
						"Your previous username was invalid. Enter a new username (May only include letters or numbers). Also, no swearing");

				match = valid(username);
			}

			conn = new Connection(new Socket(ip, 9001));
		}
		catch (UnknownHostException e)
		{
			e.printStackTrace();
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}
	}

	public static boolean valid(String username)
	{
		return username.matches(regex) && !contains(username, swears) && username.length() < 15;
	}

	public static void send(String s)
	{
		conn.sendNS(s);
	}

	public Connection(Socket s)
	{
		this.s = s;
		try
		{
			// Creates the BufferedReader and PrintWriter
			in = new BufferedReader(new InputStreamReader(s.getInputStream()));
			out = new PrintWriter(s.getOutputStream());
		}
		catch (IOException e)
		{
			e.printStackTrace();
		}

		// Creates and starts a new thread

		t = new Thread(this);
		t.start();
	}

	public String read()
	{
		try
		{
			return in.readLine();
		}
		catch (IOException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return null;
	}

	public void sendNS(String msg)
	{
		System.out.println("Sending: " + msg);
		out.println(username + ":" + msg);
		out.flush();
	}

	public void run()
	{
		while (true)
		{
			String temp = read();
			if (temp != null && !temp.equals(""))
			{
				System.out.println(temp);
				Client.newMessage(temp);
			}
		}
	}

	public static boolean contains(String s, ArrayList<String> arr)
	{
		s = s.toLowerCase();
		for (String a : arr)
		{
			if (s.contains(a))
			{
				return true;
			}
		}
		return false;
	}
}
