package client;

import java.util.Vector;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.GridPane;
import javafx.stage.Stage;
import javafx.stage.WindowEvent;

public class Client extends Application implements Runnable
{

	private static int DELAY = 5000;

	// This code is fucking disgusting

	public static void main(String[] args)
	{
		Connection.init();
		launch(args);
	}

	public static void newMessage(String msg)
	{
		System.out.println(msg);
		if (msg.startsWith("DELAYTIME"))
		{
			System.out.println(msg);
			DELAY = Integer.parseInt(msg.substring(msg.indexOf(":") + 1));
		}
	}

	// Contains all buttons so they can be disabled
	Vector<Button> v = new Vector<Button>();

	Thread thread;

	@Override
	public void start(Stage primaryStage) throws Exception
	{
		Button up = new Button();
		Button down = new Button();
		Button left = new Button();
		Button right = new Button();
		Button a = new Button();
		Button b = new Button();
		Button startB = new Button();

		buttonSetUp(a, "A");
		buttonSetUp(b, "B");
		buttonSetUp(up, "U");
		buttonSetUp(down, "D");
		buttonSetUp(left, "L");
		buttonSetUp(right, "R");
		buttonSetUp(startB, "V");

		GridPane mainPane = new GridPane();
		mainPane.add(up, 1, 0);
		mainPane.add(down, 1, 2);
		mainPane.add(left, 0, 1);
		mainPane.add(right, 2, 1);
		mainPane.add(a, 4, 1);
		mainPane.add(b, 3, 2);
		mainPane.add(startB, 1, 3, 2, 1);

		Scene scene = new Scene(mainPane, 500, 380);
		primaryStage.setScene(scene);
		primaryStage.setTitle("SummerTech Plays Pokemon Client - " + Connection.username);
		primaryStage.show();
		primaryStage.setOnCloseRequest(new EventHandler<WindowEvent>(){

			@Override
			public void handle(WindowEvent arg0) {
				// TODO Auto-generated method stub
				System.exit(0);
			}
			
		});
	}

	public void buttonSetUp(Button btn, String message)
	{
		Image im = new Image(getClass().getResourceAsStream("/Resources/" + message + ".png"));
		btn.setGraphic(new ImageView(im));
		// btn.setText(message);
		v.add(btn);

		btn.setOnAction(new EventHandler<ActionEvent>()
		{

			@Override
			public void handle(ActionEvent event)
			{
				// TODO Auto-generated method stub
				Connection.send(message); // HOOK
				disableButtons();

			}

		});

	}

	public void disableButtons()
	{
		thread = new Thread(this);
		thread.start();
	}

	@Override
	public void run()
	{
		// TODO Auto-generated method stub
		for (Button b : v)
		{
			b.setDisable(true);
		}
		try
		{
			Thread.sleep(DELAY);
		}
		catch (InterruptedException e)
		{
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		for (Button b : v)
		{
			b.setDisable(false);
		}
		thread.interrupt();
	}

}
